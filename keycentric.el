;;;; keycentric.el --- Centralizing keybindings into one place.  -*- lexical-binding: t; fill-column: 80 -*-
;;
;; Copyright (C) 2019,2020  Hai NGUYEN
;;
;; Author: Hai NGUYEN <haiuyeng@gmail.com>
;; Package-Version: 0.4.0
;; Package-Requires: ((emacs "27.1"))
;; Keywords: dotemacs startup key
;; Homepage: <https://gitlab.com/haicnguyen/keycentric-el.git>, <https://github.com/haicnguyen/keycentric-el>
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
;;
;;
;;; Commentary:
;;
;; Please refer to the attached file "README.md".
;;
;; Tested on Emacs 27.1


;;; Code:

;;; * customizable variables

(defcustom keycentric-map-filename "~/.emacs.d/keycentric/keycentric-map.el"
  "Name of the file to store keycentric keymaps.")

;;; * code

(defvar keycentric-map nil)

(defun keycentric-load ()
  "Doc."
  (let (mapsym funsym funsymtmp keystr)
    (dolist (pkginfo keycentric-map)
      (dolist (mapinfo (cdr pkginfo))
        (setq mapsym (car mapinfo))
        (and (symbolp mapsym)
             (boundp mapsym)
             (keymapp (symbol-value mapsym))
             (dolist (entry (cdr mapinfo))
               (setq keystr (elt entry 0)
                     funsym (elt entry 1))
               (if (symbolp keystr) (setq keystr (eval keystr)))
               (define-key (symbol-value mapsym) (kbd keystr) funsym)))))))

(add-hook 'after-init-hook 'keycentric-load)

(defun keycentric-modify-key (modifier keystr)
  (cond
   ((string-match "^<.+>$" keystr)
    (and (string-match "\\(M-\\|C-\\)?S" modifier)
         (setq keystr
               (pcase keystr
                 ("<kp-insert>" "<kp-0>")
                 ("<kp-delete>" "<kp-decimal>")
                 ("<kp-end>" "<kp-1>")
                 ("<kp-down>" "<kp-2>")
                 ("<kp-next>" "<kp-3>")
                 ("<kp-left>" "<kp-4>")
                 ("<kp-begin>" "<kp-5>")
                 ("<kp-home>" "<kp-7>")
                 ("<kp-right>" "<kp-6>")
                 ("<kp-up>" "<kp-8>")
                 ("<kp-prior>" "<kp-9>")
                 (_ keystr))))
    (replace-regexp-in-string "^<" (format "<%s-" modifier) keystr))
   (t (user-error "Undefined scenario where keystr is %S" keystr))))

(defun keycentric-load-entry (keymap-bindings-alist)
  (cl-loop for map-specs in keymap-bindings-alist
           for map = (car map-specs)
           if (and (boundp map) (keymapp (symbol-value map)))
           do (cl-loop
               for entry in (cdr map-specs)
               for key = (elt entry 0)
               for key-real = (if (stringp key) key (symbol-value key))
               do (define-key (symbol-value map) (kbd key-real) (elt entry 1)))
           else
           do (warn "Keymap %S is %S!" map (if (not (boundp map)) "not bound" "not a keymap"))))

(defun keycentric-define (form)
  (and (setq keycentric-map form) (keycentric-load))
  ;; add each package extra keymaps into `eval-after-load' of the mode:
  (cl-loop for pkg-specs in keycentric-map do
           ;; `eval-after-load' ensures this definition below is only defined once
	   ;; in the `after-load-alist':
           (eval-after-load (car pkg-specs) `(keycentric-load-entry ',(cdr pkg-specs)))))

(defun keycentric-goto-defun (key)
  (interactive "kPress key to find its definition: ")
  (let* ((fun (key-binding key nil 'no-remap)))
    (cond
     ((symbolp fun)
      (message "Finding definition source for %S" fun)
      (and (null (eq 'keyboard-quit fun))
           (or (ignore-errors (find-function fun))
               (imenu (symbol-name fun)))))
     ((let (grep-process)
        (grep-compute-defaults)
        ;; grep runs ASYNCHRONOUSLY:
        (setf grep-process
              (get-buffer-process
               (grep (format "%s %S %S*" (replace-regexp-in-string "-[Ee]\\>"
                                                                   "--directories=skip -E"
                                                                   grep-command)
                             (format "def[a-z]+[ \\t]+%s([ \\t;]|$)" fun)
                             (h-user-init-dir-truename)))))
        (set-process-sentinel grep-process (lambda (proc state-string)
                                             (next-error)
                                             ;; delete the window cause by
                                             ;; calling `next-error':
                                             (let ((win (get-buffer-window
                                                         (process-buffer proc))))
                                               (delete-window win))))))
     (t (user-error "Unable to find the function `%s', have you renamed the function
     name without re-defining the associated key?" fun)))))

(defun keycentric-goto-binding (key)
  "Go to the line (in `keycentric-map-filename') that define the binding of the given KEY."
  (interactive "kPress key to find its definition: ")
  (let* ((buf (find-buffer-visiting keycentric-map-filename))
         (buf-visited buf)
         (kbd-str (key-description key))
         bounds
         found)
    (or (buffer-live-p buf-visited) (setq buf (find-file keycentric-map-filename)
                                          buf-visited nil))
    (cl-assert (buffer-live-p buf))
    (with-current-buffer buf
      (setq bounds (h-toplevel-find 'keycentric-define))
      (and (consp bounds)
           (integerp (cdr bounds))
           (cl-destructuring-bind (start . end) bounds
             (save-excursion
               (goto-char start)
               ;; (and (re-search-forward (format "%S" (regexp-quote kbd-str)) end t))
               (down-list 2)
               (cl-flet ((down-list-no-error (&optional nTimes)
                            (ignore-errors (down-list nTimes) t)))
                 (let* (package-name)
                   (catch 'found-it
                     (while (funcall #'down-list-no-error) ; package
                       (setq package-name (sexp-at-point))
                       (cl-assert (null package-name) (stringp package-name))
                       (while (funcall #'down-list-no-error)
                         (while (funcall #'down-list-no-error)
                           (if (string-equal (eval (sexp-at-point)) kbd-str)
                               (progn (setq found (point))
                                      (throw 'found-it found))
                             (up-list))))))))))
           (if found
               (progn
                 (switch-to-buffer buf)
                 (goto-char found))
             (or buf-visited (setq buf-visited (buffer-name buf)) (kill-buffer buf))
             (user-error "No entry for %S found in %S!" kbd-str buf-visited))))))

;;; * load the stored keymapping:
(unless (file-exists-p keycentric-map-filename)
  (let* ((dir (file-name-directory keycentric-map-filename)))
    (mkdir dir t)
    (if (file-readable-p dir)
        (with-temp-buffer
          (write-file keycentric-map-filename))
        (custom-set-variables
         '(keycentric-map-filename nil)))))

(load keycentric-map-filename)


(provide 'keycentric)
;;; keycentric.el ends here
